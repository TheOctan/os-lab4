﻿namespace Lab4
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.PriorityStatusLabel = new System.Windows.Forms.Label();
			this.PriorityCountTasksLabel = new System.Windows.Forms.Label();
			this.PriorityAddTaskButton = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.PriorityTasksList = new System.Windows.Forms.ListBox();
			this.PriorityClassesList = new System.Windows.Forms.ListBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.CountPriorities = new System.Windows.Forms.NumericUpDown();
			this.PriorityCountTasks = new System.Windows.Forms.NumericUpDown();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.FairStatusLabel = new System.Windows.Forms.Label();
			this.CountUsersLabel = new System.Windows.Forms.Label();
			this.FairCountTasksLabel = new System.Windows.Forms.Label();
			this.AddUserButton = new System.Windows.Forms.Button();
			this.FairAddTaskButton = new System.Windows.Forms.Button();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.FairTasksList = new System.Windows.Forms.ListBox();
			this.UsersList = new System.Windows.Forms.ListBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.CountUsers = new System.Windows.Forms.NumericUpDown();
			this.FairCountTasks = new System.Windows.Forms.NumericUpDown();
			this.StartButton = new System.Windows.Forms.Button();
			this.BreakButton = new System.Windows.Forms.Button();
			this.ResetButton = new System.Windows.Forms.Button();
			this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
			this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.CountPriorities)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.PriorityCountTasks)).BeginInit();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.CountUsers)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.FairCountTasks)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.numericUpDown1);
			this.groupBox1.Controls.Add(this.PriorityStatusLabel);
			this.groupBox1.Controls.Add(this.PriorityCountTasksLabel);
			this.groupBox1.Controls.Add(this.PriorityAddTaskButton);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.PriorityTasksList);
			this.groupBox1.Controls.Add(this.PriorityClassesList);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.CountPriorities);
			this.groupBox1.Controls.Add(this.PriorityCountTasks);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(425, 412);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Priority planner";
			// 
			// PriorityStatusLabel
			// 
			this.PriorityStatusLabel.AutoSize = true;
			this.PriorityStatusLabel.Location = new System.Drawing.Point(279, 24);
			this.PriorityStatusLabel.Name = "PriorityStatusLabel";
			this.PriorityStatusLabel.Size = new System.Drawing.Size(76, 13);
			this.PriorityStatusLabel.TabIndex = 9;
			this.PriorityStatusLabel.Text = "Status: waiting";
			// 
			// PriorityCountTasksLabel
			// 
			this.PriorityCountTasksLabel.AutoSize = true;
			this.PriorityCountTasksLabel.Location = new System.Drawing.Point(225, 24);
			this.PriorityCountTasksLabel.Name = "PriorityCountTasksLabel";
			this.PriorityCountTasksLabel.Size = new System.Drawing.Size(48, 13);
			this.PriorityCountTasksLabel.TabIndex = 8;
			this.PriorityCountTasksLabel.Text = "Tasks: 0";
			// 
			// PriorityAddTaskButton
			// 
			this.PriorityAddTaskButton.Location = new System.Drawing.Point(144, 19);
			this.PriorityAddTaskButton.Name = "PriorityAddTaskButton";
			this.PriorityAddTaskButton.Size = new System.Drawing.Size(75, 23);
			this.PriorityAddTaskButton.TabIndex = 7;
			this.PriorityAddTaskButton.Text = "Add Task";
			this.PriorityAddTaskButton.UseVisualStyleBackColor = true;
			this.PriorityAddTaskButton.Click += new System.EventHandler(this.PriorityAddTaskButton_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(301, 71);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(36, 13);
			this.label6.TabIndex = 6;
			this.label6.Text = "Tasks";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(6, 71);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(76, 13);
			this.label5.TabIndex = 6;
			this.label5.Text = "Priority classes";
			// 
			// PriorityTasksList
			// 
			this.PriorityTasksList.FormattingEnabled = true;
			this.PriorityTasksList.Location = new System.Drawing.Point(304, 87);
			this.PriorityTasksList.Name = "PriorityTasksList";
			this.PriorityTasksList.Size = new System.Drawing.Size(115, 316);
			this.PriorityTasksList.TabIndex = 5;
			// 
			// PriorityClassesList
			// 
			this.PriorityClassesList.FormattingEnabled = true;
			this.PriorityClassesList.Location = new System.Drawing.Point(9, 87);
			this.PriorityClassesList.Name = "PriorityClassesList";
			this.PriorityClassesList.Size = new System.Drawing.Size(289, 316);
			this.PriorityClassesList.TabIndex = 4;
			this.PriorityClassesList.SelectedIndexChanged += new System.EventHandler(this.PriorityClassesList_SelectedIndexChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 47);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(76, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Count priorities";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 21);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(63, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Count tasks";
			// 
			// CountPriorities
			// 
			this.CountPriorities.Location = new System.Drawing.Point(88, 45);
			this.CountPriorities.Name = "CountPriorities";
			this.CountPriorities.Size = new System.Drawing.Size(50, 20);
			this.CountPriorities.TabIndex = 1;
			this.CountPriorities.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
			this.CountPriorities.ValueChanged += new System.EventHandler(this.CountPriorities_ValueChanged);
			// 
			// PriorityCountTasks
			// 
			this.PriorityCountTasks.Location = new System.Drawing.Point(88, 19);
			this.PriorityCountTasks.Name = "PriorityCountTasks";
			this.PriorityCountTasks.Size = new System.Drawing.Size(50, 20);
			this.PriorityCountTasks.TabIndex = 0;
			this.PriorityCountTasks.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.PriorityCountTasks.ValueChanged += new System.EventHandler(this.PriorityCountTasks_ValueChanged);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.numericUpDown2);
			this.groupBox2.Controls.Add(this.FairStatusLabel);
			this.groupBox2.Controls.Add(this.CountUsersLabel);
			this.groupBox2.Controls.Add(this.FairCountTasksLabel);
			this.groupBox2.Controls.Add(this.AddUserButton);
			this.groupBox2.Controls.Add(this.FairAddTaskButton);
			this.groupBox2.Controls.Add(this.label8);
			this.groupBox2.Controls.Add(this.label7);
			this.groupBox2.Controls.Add(this.FairTasksList);
			this.groupBox2.Controls.Add(this.UsersList);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Controls.Add(this.CountUsers);
			this.groupBox2.Controls.Add(this.FairCountTasks);
			this.groupBox2.Location = new System.Drawing.Point(443, 12);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(420, 412);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Fair planner";
			// 
			// FairStatusLabel
			// 
			this.FairStatusLabel.AutoSize = true;
			this.FairStatusLabel.Location = new System.Drawing.Point(267, 25);
			this.FairStatusLabel.Name = "FairStatusLabel";
			this.FairStatusLabel.Size = new System.Drawing.Size(76, 13);
			this.FairStatusLabel.TabIndex = 12;
			this.FairStatusLabel.Text = "Status: waiting";
			// 
			// CountUsersLabel
			// 
			this.CountUsersLabel.AutoSize = true;
			this.CountUsersLabel.Location = new System.Drawing.Point(212, 47);
			this.CountUsersLabel.Name = "CountUsersLabel";
			this.CountUsersLabel.Size = new System.Drawing.Size(46, 13);
			this.CountUsersLabel.TabIndex = 11;
			this.CountUsersLabel.Text = "Users: 0";
			// 
			// FairCountTasksLabel
			// 
			this.FairCountTasksLabel.AutoSize = true;
			this.FairCountTasksLabel.Location = new System.Drawing.Point(212, 24);
			this.FairCountTasksLabel.Name = "FairCountTasksLabel";
			this.FairCountTasksLabel.Size = new System.Drawing.Size(48, 13);
			this.FairCountTasksLabel.TabIndex = 10;
			this.FairCountTasksLabel.Text = "Tasks: 0";
			// 
			// AddUserButton
			// 
			this.AddUserButton.Location = new System.Drawing.Point(131, 45);
			this.AddUserButton.Name = "AddUserButton";
			this.AddUserButton.Size = new System.Drawing.Size(75, 23);
			this.AddUserButton.TabIndex = 9;
			this.AddUserButton.Text = "Add User";
			this.AddUserButton.UseVisualStyleBackColor = true;
			this.AddUserButton.Click += new System.EventHandler(this.AddUserButton_Click);
			// 
			// FairAddTaskButton
			// 
			this.FairAddTaskButton.Location = new System.Drawing.Point(131, 19);
			this.FairAddTaskButton.Name = "FairAddTaskButton";
			this.FairAddTaskButton.Size = new System.Drawing.Size(75, 23);
			this.FairAddTaskButton.TabIndex = 8;
			this.FairAddTaskButton.Text = "Add Task";
			this.FairAddTaskButton.UseVisualStyleBackColor = true;
			this.FairAddTaskButton.Click += new System.EventHandler(this.FairAddTaskButton_Click);
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(302, 71);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(36, 13);
			this.label8.TabIndex = 7;
			this.label8.Text = "Tasks";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(6, 71);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(34, 13);
			this.label7.TabIndex = 6;
			this.label7.Text = "Users";
			// 
			// FairTasksList
			// 
			this.FairTasksList.FormattingEnabled = true;
			this.FairTasksList.Location = new System.Drawing.Point(305, 87);
			this.FairTasksList.Name = "FairTasksList";
			this.FairTasksList.Size = new System.Drawing.Size(109, 316);
			this.FairTasksList.TabIndex = 5;
			// 
			// UsersList
			// 
			this.UsersList.FormattingEnabled = true;
			this.UsersList.Location = new System.Drawing.Point(6, 87);
			this.UsersList.Name = "UsersList";
			this.UsersList.Size = new System.Drawing.Size(293, 316);
			this.UsersList.TabIndex = 4;
			this.UsersList.SelectedIndexChanged += new System.EventHandler(this.UsersList_SelectedIndexChanged);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(6, 47);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(63, 13);
			this.label4.TabIndex = 3;
			this.label4.Text = "Count users";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(6, 21);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(63, 13);
			this.label3.TabIndex = 2;
			this.label3.Text = "Count tasks";
			// 
			// CountUsers
			// 
			this.CountUsers.Location = new System.Drawing.Point(75, 45);
			this.CountUsers.Name = "CountUsers";
			this.CountUsers.Size = new System.Drawing.Size(50, 20);
			this.CountUsers.TabIndex = 1;
			this.CountUsers.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.CountUsers.ValueChanged += new System.EventHandler(this.CountUsers_ValueChanged);
			// 
			// FairCountTasks
			// 
			this.FairCountTasks.Location = new System.Drawing.Point(75, 19);
			this.FairCountTasks.Name = "FairCountTasks";
			this.FairCountTasks.Size = new System.Drawing.Size(50, 20);
			this.FairCountTasks.TabIndex = 0;
			this.FairCountTasks.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.FairCountTasks.ValueChanged += new System.EventHandler(this.FairCountTasks_ValueChanged);
			// 
			// StartButton
			// 
			this.StartButton.Location = new System.Drawing.Point(13, 430);
			this.StartButton.Name = "StartButton";
			this.StartButton.Size = new System.Drawing.Size(75, 23);
			this.StartButton.TabIndex = 2;
			this.StartButton.Text = "Start";
			this.StartButton.UseVisualStyleBackColor = true;
			this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
			// 
			// BreakButton
			// 
			this.BreakButton.Enabled = false;
			this.BreakButton.Location = new System.Drawing.Point(95, 429);
			this.BreakButton.Name = "BreakButton";
			this.BreakButton.Size = new System.Drawing.Size(75, 23);
			this.BreakButton.TabIndex = 3;
			this.BreakButton.Text = "Break";
			this.BreakButton.UseVisualStyleBackColor = true;
			this.BreakButton.Click += new System.EventHandler(this.BreakButton_Click);
			// 
			// ResetButton
			// 
			this.ResetButton.Enabled = false;
			this.ResetButton.Location = new System.Drawing.Point(176, 430);
			this.ResetButton.Name = "ResetButton";
			this.ResetButton.Size = new System.Drawing.Size(75, 23);
			this.ResetButton.TabIndex = 4;
			this.ResetButton.Text = "Reset";
			this.ResetButton.UseVisualStyleBackColor = true;
			this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
			// 
			// numericUpDown1
			// 
			this.numericUpDown1.Location = new System.Drawing.Point(145, 47);
			this.numericUpDown1.Name = "numericUpDown1";
			this.numericUpDown1.Size = new System.Drawing.Size(74, 20);
			this.numericUpDown1.TabIndex = 10;
			this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
			// 
			// numericUpDown2
			// 
			this.numericUpDown2.Location = new System.Drawing.Point(270, 45);
			this.numericUpDown2.Name = "numericUpDown2";
			this.numericUpDown2.Size = new System.Drawing.Size(78, 20);
			this.numericUpDown2.TabIndex = 13;
			this.numericUpDown2.ValueChanged += new System.EventHandler(this.numericUpDown2_ValueChanged);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.ClientSize = new System.Drawing.Size(875, 465);
			this.Controls.Add(this.ResetButton);
			this.Controls.Add(this.BreakButton);
			this.Controls.Add(this.StartButton);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.MaximizeBox = false;
			this.Name = "Form1";
			this.ShowIcon = false;
			this.Text = "Planners";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.CountPriorities)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.PriorityCountTasks)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.CountUsers)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.FairCountTasks)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button StartButton;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown CountPriorities;
		private System.Windows.Forms.NumericUpDown PriorityCountTasks;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown CountUsers;
		private System.Windows.Forms.NumericUpDown FairCountTasks;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ListBox PriorityTasksList;
		private System.Windows.Forms.ListBox PriorityClassesList;
		private System.Windows.Forms.ListBox FairTasksList;
		private System.Windows.Forms.ListBox UsersList;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Button PriorityAddTaskButton;
		private System.Windows.Forms.Button AddUserButton;
		private System.Windows.Forms.Button FairAddTaskButton;
		private System.Windows.Forms.Button BreakButton;
		private System.Windows.Forms.Label PriorityCountTasksLabel;
		private System.Windows.Forms.Label FairCountTasksLabel;
		private System.Windows.Forms.Label CountUsersLabel;
		private System.Windows.Forms.Label PriorityStatusLabel;
		private System.Windows.Forms.Label FairStatusLabel;
		private System.Windows.Forms.Button ResetButton;
		private System.Windows.Forms.NumericUpDown numericUpDown1;
		private System.Windows.Forms.NumericUpDown numericUpDown2;
	}
}

