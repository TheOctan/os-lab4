﻿using Lab4.Planner.Fair;
using Lab4.Planner.Priority;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

// Вариант 14 => 5 : 9

//	5. Приоритетное планирование
// Каждому процессу присваивается значение приоритетности,
// и запускается тот процесс, который находится в состоянии готовности и имеет наивысший приоритет.
// При реализации необходимо предусмотреть следующие функции:
// > выбор количества заданий в очереди (от 2 до 10);
// > запуск и остановка процесса выполнения;
// > выбор количества классов приоритетов(от 3 до 10);
// > выбор размера кванта времени выполнения;
// > назначение времени выполнения для каждого задания;
// > назначение приоритета для каждого задания;

// 9. Справедливое планирование
// В этой модели каждому пользователю распределяется некоторая доля процессорного времени и планировщик выбирает процессы, соблюдая это распределение.
// При реализации необходимо предусмотреть следующие функции:
// > выбор количества заданий (для каждого пользователя) в очереди(от 2 до 10);
// > выбор количества пользователей(от 2 до 5);
// > запуск и остановка процесса выполнения;
// > выбор размера кванта времени выполнения;
// > назначение времени выполнения для каждого задания;

namespace Lab4
{
	public partial class Form1 : Form
	{
		private PriorityPlanner priorityPlanner = new PriorityPlanner();
		private FairPlanner fairPlanner = new FairPlanner();
		private Random random = new Random();

		private Thread thread1;
		private Thread thread2;

		public Form1()
		{
			InitializeComponent();

			priorityPlanner.Started += PriorityPlanner_Started;
			priorityPlanner.Completed += PriorityPlanner_Completed;

			fairPlanner.Started += FairPlanner_Started;
			fairPlanner.Completed += FairPlanner_Completed;
		}

		private void PriorityPlanner_Started(object sender, EventArgs e)
		{
			Action action = () => PriorityStatusLabel.Text = "Status: started";
			Invoke(action);
		}
		private void PriorityPlanner_Completed(object sender, EventArgs e)
		{
			Action action = () => PriorityStatusLabel.Text = "Status: completed";
			action += delegate ()
			{
				if (fairPlanner.IsCompleted)
				{
					BreakButton.Enabled = false;
					ResetButton.Enabled = true;
				}
			};

			Invoke(action);
		}
		private void FairPlanner_Started(object sender, EventArgs e)
		{
			Action action = () => FairStatusLabel.Text = "Status: started";
			Invoke(action);
		}
		private void FairPlanner_Completed(object sender, EventArgs e)
		{
			Action action = () => FairStatusLabel.Text = "Status: completed";
			action += delegate ()
			{
				if (priorityPlanner.IsCompleted)
				{
					BreakButton.Enabled = false;
					ResetButton.Enabled = true;
				}
			};

			Invoke(action);
		}

		private void StartButton_Click(object sender, EventArgs e)
		{
			if (sender is Button button) button.Enabled = false;
			BreakButton.Enabled = true;

			thread1 = new Thread(priorityPlanner.Run);
			thread2 = new Thread(fairPlanner.Run);

			thread1.Start();
			thread2.Start();
		}
		private void BreakButton_Click(object sender, EventArgs e)
		{
			if (sender is Button button) button.Enabled = false;
			ResetButton.Enabled = true;

			thread1.Abort();
			thread2.Abort();

			PriorityStatusLabel.Text = "Status: breaked";
			FairStatusLabel.Text = "Status: breaked";
		}
		private void ResetButton_Click(object sender, EventArgs e)
		{
			if (sender is Button button) button.Enabled = false;
			StartButton.Enabled = true;

			priorityPlanner.Reset();
			fairPlanner.Reset();

			UpdatePriorityList();
			UpdateFairList();
			PriorityTasksList.Items.Clear();
			FairTasksList.Items.Clear();

			FairCountTasksLabel.Text = "Tasks: " + fairPlanner.CountTasks.ToString();
		}

		private void PriorityCountTasks_ValueChanged(object sender, EventArgs e)
		{
			CheckRange(PriorityCountTasks, 2, 10);
		}
		private void CountPriorities_ValueChanged(object sender, EventArgs e)
		{
			CheckRange(CountPriorities, 3, 10);
		}
		private void FairCountTasks_ValueChanged(object sender, EventArgs e)
		{
			CheckRange(FairCountTasks, 2, 10);
		}
		private void CountUsers_ValueChanged(object sender, EventArgs e)
		{
			CheckRange(CountUsers, 2, 5);
		}

		private void CheckRange(NumericUpDown numeric, int lower, int upper)
		{
			if (numeric.Value < lower)
			{
				numeric.Value = lower;
			}
			else if (numeric.Value > upper)
			{
				numeric.Value = upper;
			}
		}

		private void UpdatePriorityList()
		{
			PriorityClassesList.Items.Clear();

			foreach (var item in priorityPlanner.ClassPriorities)
			{
				PriorityClassesList.Items.Add(item);
			}

			PriorityCountTasksLabel.Text = "Tasks: " + priorityPlanner.CountTasks.ToString();
		}
		private void UpdateFairList()
		{
			UsersList.Items.Clear();

			foreach (var item in fairPlanner.Users)
			{
				UsersList.Items.Add(item);
			}

			CountUsersLabel.Text = "Users: " + fairPlanner.CountUsers.ToString();
		}
		private void UpdateTaskList(ListBox list, IEnumerable<Planner.Task> tasks)
		{
			list.Items.Clear();

			foreach (var task in tasks)
			{
				list.Items.Add(task);
			}
		}

		private void PriorityAddTaskButton_Click(object sender, EventArgs e)
		{
			if (priorityPlanner.CountTasks >= PriorityCountTasks.Value)
			{
				MessageBox.Show("Max count of Tasks");
				return;
			}

			var task = new Planner.Task(
					TimeSpan.FromMilliseconds(1000),
					random.Next(1, (int)CountPriorities.Value + 1)
					);

			priorityPlanner.AddTask(task);

			UpdatePriorityList();
		}
		private void FairAddTaskButton_Click(object sender, EventArgs e)
		{
			if (fairPlanner.CountTasks >= FairCountTasks.Value)
			{
				MessageBox.Show("Max count of Tasks");
				return;
			}

			if (fairPlanner.CountUsers == 0)
			{
				MessageBox.Show("You must add Users");
			}
			else
			{
				if (UsersList.SelectedIndex != -1)
				{
					var task = new Planner.Task(TimeSpan.FromMilliseconds(1000));

					fairPlanner.AddTask(task, UsersList.SelectedIndex);

					UpdateTaskList(FairTasksList, ((User)UsersList.SelectedItem).Tasks);
				}
				else
				{
					MessageBox.Show("You must select a User");
				}
			}

			FairCountTasksLabel.Text = "Tasks: " + fairPlanner.CountTasks.ToString();
		}
		private void AddUserButton_Click(object sender, EventArgs e)
		{
			if (fairPlanner.CountUsers >= CountUsers.Value)
			{
				MessageBox.Show("Max count of Users");
				return;
			}

			fairPlanner.AddUser(new User());

			UpdateFairList();
		}

		private void PriorityClassesList_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (sender is ListBox list)
			{
				if (list.SelectedItem is ClassPriority classPriority)
				{
					UpdateTaskList(PriorityTasksList, classPriority.Tasks);
				}
			}
		}
		private void UsersList_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (sender is ListBox list)
			{
				if (list.SelectedItem is User user)
				{
					UpdateTaskList(FairTasksList, user.Tasks);
				}
			}
		}

		private void numericUpDown1_ValueChanged(object sender, EventArgs e)
		{
			PriorityTimeQuantum.TotalMilliseconds = (double)numericUpDown1.Value;
		}

		private void numericUpDown2_ValueChanged(object sender, EventArgs e)
		{
			FairTimeQuantum.TotalMilliseconds = (double)numericUpDown2.Value;
		}
	}
}
