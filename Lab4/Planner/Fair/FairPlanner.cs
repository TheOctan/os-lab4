﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Planner.Fair
{
	public class FairPlanner : IPlanner, IUserPlanner
	{
		public event EventHandler Completed = (object sender, EventArgs e) => { };
		public event EventHandler Started = (object sender, EventArgs e) => { };

		public IEnumerable<User> Users => users;
		public int CountTasks => users.Sum(x => x.CountTasks);
		public int CountUsers => users.Count;
		public bool IsCompleted { get; private set; }

		private List<User> users = new List<User>();
		private readonly Stopwatch timer = new Stopwatch();
		private int currentUser = 0;

		private bool IsEmpty()
		{
			foreach (var user in users)
			{
				if (!user.IsEmpty)
					return false;
			}

			return true;
		}

		public FairPlanner()
		{
			IsCompleted = false;
		}

		public void AddTask(Task task)
		{
			if (currentUser >= users.Count)
				currentUser = 0;

			users[currentUser++].AddTask(task);
		}
		public void AddTask(Task task, int userNumber)
		{
			if (userNumber < 0 || userNumber >= users.Count)
				throw new IndexOutOfRangeException();

			users[userNumber].AddTask(task);
		}
		public void AddUser(User user)
		{
			users.Add(user);
		}

		public void Run()
		{
			Started(this, EventArgs.Empty);

			while (!IsEmpty())
			{
				foreach (var user in users)
				{
					if (user.IsEmpty)
						continue;

					var task = user.GetNextTask();

					if (task != null)
					{
						if (task.IsCompleted)
						{
							user.DeleteCurrentTask();
						}
						else
							task.Run(timer, FairTimeQuantum.TotalMilliseconds);
					}
				}
			}

			IsCompleted = true;
			Completed(this, EventArgs.Empty);
		}
		public void Reset()
		{
			IsCompleted = false;
			users.Clear();
		}
	}
}
