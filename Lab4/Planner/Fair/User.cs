﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Planner.Fair
{
	public class User
	{
		public IEnumerable<Task> Tasks => tasks;
		public int CountTasks => tasks.Count;
		public bool IsEmpty => tasks.Count == 0;
		public Guid ID { get; }

		private List<Task> tasks = new List<Task>();
		private int currentTask = -1;

		public User()
		{
			ID = Guid.NewGuid();
		}

		public void AddTask(Task task)
		{
			tasks.Add(task);
		}

		public Task GetNextTask()
		{
			currentTask++;

			if (currentTask >= tasks.Count)
				currentTask = 0;

			if (tasks.Count != 0)
				return tasks[currentTask];
			else
				return null;
		}

		public void DeleteCurrentTask()
		{
			tasks.RemoveAt(currentTask);
		}

		public override string ToString()
		{
			return "User: " + ID.ToString();
		}
	}
}
