﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Planner
{
	public interface IPlanner
	{
		void AddTask(Task task);		

		void Run();
	}
}
