﻿using Lab4.Planner.Fair;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Planner
{
	interface IUserPlanner
	{
		void AddTask(Task task, int userNumber);
		void AddUser(User user);

		void Run();
	}
}
