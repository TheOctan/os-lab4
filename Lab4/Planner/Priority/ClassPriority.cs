﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Planner.Priority
{
	public class ClassPriority : IComparable<ClassPriority>
	{
		public int CountTasks => tasks.Count;
		public int Priority { get; }
		public bool IsCompleted => tasks.Count == 0;
		public IEnumerable<Task> Tasks => tasks;

		private List<Task> tasks = new List<Task>();

		public ClassPriority(int priority)
		{
			Priority = priority;
		}

		public void AddTask(Task task)
		{
			if (task.Priority == this.Priority)
				tasks.Add(task);
		}

		public void Run(Stopwatch timer)
		{
			while (!IsCompleted)
			{
				for (int i = 0; i < tasks.Count;)
				{
					var task = tasks[i];

					if (task.IsCompleted)
					{
						tasks.RemoveAt(i);
					}
					else
					{
						task.Run(timer, PriorityTimeQuantum.TotalMilliseconds);
						i++;
					}
				}
			}
		}

		public int CompareTo(ClassPriority other)
		{
			return Priority.CompareTo(other.Priority);
		}

		public override string ToString()
		{
			return "Priority Class " + Priority.ToString();
		}
	}
}
