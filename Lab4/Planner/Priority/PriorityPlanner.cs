﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Planner.Priority
{
	public class PriorityPlanner : IPlanner
	{
		public event EventHandler Completed = (object sender, EventArgs e) => { };
		public event EventHandler Started = (object sender, EventArgs e) => { };

		public IEnumerable<ClassPriority> ClassPriorities => classPriorities;
		public int CountTasks => ClassPriorities.Sum(x => x.CountTasks);
		public bool IsCompleted { get; private set; }

		private SortedSet<ClassPriority> classPriorities = new SortedSet<ClassPriority>();
		private readonly Stopwatch timer = new Stopwatch();

		public PriorityPlanner()
		{
			IsCompleted = false;
		}

		public void AddTask(Task task)
		{
			var classPriority = classPriorities.SingleOrDefault(x => x.Priority == task.Priority);

			if(classPriority == null)
			{
				classPriority = new ClassPriority(task.Priority);
				classPriorities.Add(classPriority);
			}

			classPriority.AddTask(task);
		}

		public void Run()
		{
			Started(this, EventArgs.Empty);

			foreach (var classPriority in classPriorities)
			{
				classPriority.Run(timer);
			}

			IsCompleted = true;
			Completed(this, EventArgs.Empty);
		}
		public void Reset()
		{
			IsCompleted = false;
			classPriorities.Clear();
		}
	}
}
