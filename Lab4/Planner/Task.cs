﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Planner
{
	public class Task
	{
		private readonly TimeSpan runTime;
		private TimeSpan currentTime;

		public Guid ID { get; }
		public int Priority { get; set; }

		public bool IsRuning => currentTime.TotalMilliseconds != 0;
		public bool IsCompleted => currentTime.TotalMilliseconds >= runTime.TotalMilliseconds;

		public Task(TimeSpan time) : this(time, 0)
		{

		}
		public Task(TimeSpan time, int priority)
		{
			runTime = time;
			currentTime = TimeSpan.Zero;

			Priority = priority;

			ID = Guid.NewGuid();
		}

		public void Run(Stopwatch timer, double timeQuantum)
		{
			if (IsCompleted) return;

			timer.Reset();
			timer.Start();

			while (timer.ElapsedMilliseconds < timeQuantum)
			{

			}

			currentTime += timer.Elapsed;
			timer.Stop();
		}

		public void Reset()
		{
			currentTime = TimeSpan.Zero;
		}

		public override string ToString()
		{
			return "Task: " + Priority.ToString();
		}
	}
}
